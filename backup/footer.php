<div class="clear"></div>
</div><!-- #main .wrapper -->
<div class="clear"></div>
  <?php if( !is_page_template('page-templates/blank-page-template.php') ): ?>
  <footer id="colophon">
    <div class="footer-container">
      <?php if( is_active_sidebar('footer-widget-area') ): ?>
      <div class="first-footer-area footer-area">
        <div class="container no-padding">
          <div class="ts-col-24">
            <?php dynamic_sidebar('footer-widget-area'); ?>
          </div>
        </div>
      </div>
      <?php endif; ?>
      
      <?php if( is_active_sidebar('footer-copyright-widget-area') ): ?>
      <div class="end-footer footer-area">
        <div class="container no-padding">
          <div class="ts-col-24">
            <?php dynamic_sidebar('footer-copyright-widget-area'); ?>
          </div>
        </div>
      </div>
      <?php endif; ?>
    </div>
  </footer>
  <?php endif; ?>
</div><!-- #page -->

<?php 
global $boxshop_theme_options;
if( ( !wp_is_mobile() && $boxshop_theme_options['ts_back_to_top_button'] ) || ( wp_is_mobile() && $boxshop_theme_options['ts_back_to_top_button_on_mobile'] ) ): 
?>
<div id="to-top" class="scroll-button">
  <a class="scroll-button" href="javascript:void(0)" title="<?php esc_attr_e('Back to Top', 'boxshop'); ?>"><?php esc_html_e('Back to Top', 'boxshop'); ?></a>
</div>
<?php endif; ?>

<?php wp_footer(); ?>

<!-- Query Box -->
<div class="query_box_wrapper">
  <form id="inquiryform" method="post" action="<?php echo get_site_url().'/request-quotations'; ?>">
  <input type="hidden" name="products" >
  <div class="query_count">
    0
  </div>
  <div class="query_box" onclick="submitForm();">
    Quote Inquiries
  </div>
</form>
</div>
<script type="text/javascript">
  jQuery(document).ready(function(){

    var rQproducts = jQuery.cookie("quotation_requested_products");
    var pCounts = 0;
    if(rQproducts){
      var pElements =  rQproducts.split(",");
      pCounts = pElements.length;
    } 
    jQuery('div.query_box_wrapper form input[name="products"]').val(rQproducts);

    jQuery("div.query_box_wrapper div.query_count").html(pCounts);

  });
</script>
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script> -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/js/jquery.cookie.js'; ?>"></script>

<script type="text/javascript">


  function submitForm(){

    jQuery("#inquiryform").submit();

  }


  function add_to_quote_list(eventVar){

    var product_id = jQuery(eventVar.target).next().attr("value");
    
    if(product_id){
      var cookieVal = jQuery.cookie("quotation_requested_products");
      if(cookieVal){
        var cookieArray = cookieVal.split(",");
        cookieArray[cookieArray.length] = product_id;
      }
      var insert = 1;
      if(cookieVal){
        if(!cookieVal.includes(product_id)){
          jQuery.cookie("quotation_requested_products",cookieArray, { expires: 7, path: '/' });
          var count = parseInt(jQuery("div.query_count").html());
          count = count +1;
          jQuery("div.query_count").html(count);
        }
        insert = 0;
      }
      
      

      if(insert){
        jQuery.cookie("quotation_requested_products",[product_id], { expires: 7, path: '/' });
        var count = parseInt(jQuery("div.query_count").html());
        count = count +1;
        jQuery("div.query_count").html(count);

      }
    }

    
    
  }


  
</script>




</body>
</html>