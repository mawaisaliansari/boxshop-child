<?php session_start();
/**
 *	Template Name: SendMail Page
 */
global $boxshop_page_datas, $boxshop_theme_options;
get_header();

$extra_class = "";

$show_breadcrumb = (!is_home() && !is_front_page() && isset($boxshop_page_datas['ts_show_breadcrumb']) && absint($boxshop_page_datas['ts_show_breadcrumb']) == 1);
$show_page_title = (!is_home() && !is_front_page() && absint($boxshop_page_datas['ts_show_page_title']) == 1);

if (($show_breadcrumb || $show_page_title) && isset($boxshop_theme_options['ts_breadcrumb_layout'])) {
  $extra_class = 'show_breadcrumb_' . $boxshop_theme_options['ts_breadcrumb_layout'];
}


boxshop_breadcrumbs_title($show_breadcrumb, $show_page_title, get_the_title());

?>
<div class="page-template fullwidth-template <?php echo esc_attr($extra_class) ?>">
  <!-- Page slider -->
  <?php if ($boxshop_page_datas['ts_page_slider'] && $boxshop_page_datas['ts_page_slider_position'] == 'before_main_content') : ?>
    <div class="top-slideshow">
      <div class="top-slideshow-wrapper">
        <?php boxshop_show_page_slider(); ?>
      </div>
    </div>
  <?php endif; ?>

  <div class="page-fullwidth-template">

    <!-- Main Content -->

    <div id="main-content" class="<?php echo esc_attr($page_column_class['main_class']); ?>">
      <div id="primary" class="site-content">

        <?php

        global $_SESSION;

        
        $to = $_POST['receiverEmail'];
        $subject = 'These Quote Requests Are Sent By  ' . $_POST['senderName'];
        $from = 'ansaripakistan7172@gmail.com';
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Create email headers
        $headers .= 'From: ' . $from . "\r\n" .
          'Reply-To: ' . $from . "\r\n" .
          'X-Mailer: PHP/' . phpversion();

        if (empty($_POST['products']) and !empty($_SESSION["quote_products"])) {
          $_POST["products"] = $_SESSION["quote_products"];
        }

        if (!empty($_POST)) {

          $products = $_POST["products"];
          $_SESSION["quote_products"] = $products;

          $productsArray = explode(',', $products);
         echo "<pre>";
         print_r($productsArray);
         exit;


          $productsData = get_posts(array("include" => $productsArray, 'post_type' => 'product'));
          if (!empty($productsData)) {
            $message = '<table>';
            $message .= '<tr style="border: 1px solid black;border-collapse: collapse;">';
              $message .= '<th>ID</th>';
              $message .= '<th>Image</th>';
              $message .= '<th>Name</th>';
              $message .= '<th>Quantity</th>';
              $message .= '</tr>';
            
            foreach ($productsData as $key => $value) {

              $pid = $value->ID;
              $title = $value->post_title;
              $thumbnail_url = get_the_post_thumbnail_url($value, array(100, 100));
            
              
              $message .= '<tr style="border: 1px solid black;border-collapse: collapse;">';
              $message .= '<td>' . $key . '</td>';
              $message .= '<td><img style="width:50px" src="' . $thumbnail_url . '"/></td>';
              $message .= '<td style="font-size="12px">' . $title . '</td>';
              $message .= '<td>' . $qtyData . '</td>';
              $message .= '</tr>';
            }
         
            $message .= '</table>';
          }
        }
        // Sending email
        if (wp_mail($to, $subject, $message, $headers)) {
          echo '<h1  style="text-align:center; color:green;">Email sent successfully to '.$to.'<br> Sender Name: '.$_POST['senderName'].'</h1>';

          unset($_SESSION['quote_products']);
         // echo '<a class="btnRemoveQuotes" value="Remove Quotes List">Remove Cookies</a>';
        } else {
          echo '<h1  style="text-align:center; color:red;">Unable to Send email. Please try again.</h1>';
        }

        ?>

      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() . '/js/jquery.cookie.js'; ?>"></script>

<script type="text/javascript">
// jQuery("div a.btnRemoveQuotes").on("click", function() {
 // jQuery("div.site-content").find("h1").css({"color":"green"});

 //   jQuery.removeCookie('quotation_requested_products', {path: '/' });
 //   jQuery(location).attr('href', '<?php echo get_site_url() . '/online-shopping-in-pakistan-industrial-consumables-7mart-pk' ?>');
  

//  });
</script>
