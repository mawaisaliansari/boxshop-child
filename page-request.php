<?php

/* Template Name: RequestQuotation */
global $boxshop_page_datas, $boxshop_theme_options;
session_start();
get_header();
$rowTable = '';



$extra_class = "";

$page_column_class = boxshop_page_layout_columns_class($boxshop_page_datas['ts_page_layout']);

$show_breadcrumb = (!is_home() && !is_front_page() && isset($boxshop_page_datas['ts_show_breadcrumb']) && absint($boxshop_page_datas['ts_show_breadcrumb']) == 1);
$show_page_title = (!is_home() && !is_front_page() && absint($boxshop_page_datas['ts_show_page_title']) == 1);
if (function_exists('is_bbpress') && is_bbpress()) {
  $show_page_title = true;
  $show_breadcrumb = true;
}
if (($show_breadcrumb || $show_page_title) && isset($boxshop_theme_options['ts_breadcrumb_layout'])) {
  $extra_class = 'show_breadcrumb_' . $boxshop_theme_options['ts_breadcrumb_layout'];
}

boxshop_breadcrumbs_title($show_breadcrumb, $show_page_title, get_the_title());

?>
<!-- Page slider -->
<?php if ($boxshop_page_datas['ts_page_slider'] && $boxshop_page_datas['ts_page_slider_position'] == 'before_main_content') : ?>
  <div class="top-slideshow">
    <div class="top-slideshow-wrapper">
      <?php boxshop_show_page_slider(); ?>
    </div>
  </div>
<?php endif; ?>

<div class="page-container <?php echo esc_attr($extra_class) ?>">

  <!-- Left Sidebar -->
  <?php if ($page_column_class['left_sidebar']) : ?>
    <aside id="left-sidebar" class="ts-sidebar <?php echo esc_attr($page_column_class['left_sidebar_class']); ?>">
      <?php if (is_active_sidebar($boxshop_page_datas['ts_left_sidebar'])) : ?>
        <?php dynamic_sidebar($boxshop_page_datas['ts_left_sidebar']); ?>
      <?php endif; ?>
    </aside>
  <?php endif; ?>

  <!-- Main Content -->
  <div id="main-content" class="<?php echo esc_attr($page_column_class['main_class']); ?>">
    <div id="primary" class="site-content">
      <form method="POST" id="formID" action="<?php echo get_site_url().'/send-mail'; ?>" style="padding:5%;">

        <?php
        global $_SESSION;



        if (empty($_POST['products']) and !empty($_SESSION["quote_products"])) {
          $_POST["products"] = $_SESSION["quote_products"];
        }

        if (!empty($_POST)) {

          $products = $_POST["products"];

          $_SESSION["quote_products"] = $products;

          $productsArray = explode(',', $products);



          $productsData = get_posts(array("include" => $productsArray, 'post_type' => 'product'));
        ?>
          <table>
            <!-- <tr>
          <th>ID</th>
          <th>Image</th>
          <th>Name</th>
          <th>Quantity</td>
          <ths>Action</td>
        </tr>  -->
            <?php



            if (!empty($productsData)) {
              foreach ($productsData as $key => $value) {

                $pid = $value->ID;
                $title = $value->post_title;
                $thumbnail_url = get_the_post_thumbnail_url($value, array(100, 100));
            ?>

                <tr class="tableData">
                  <td><?php echo $key + 1 ?></td>
                  <td><img src="<?php echo $thumbnail_url ?>" /></td>
                  <td><?php echo $title ?></td>
                  <td>
                    <input type="number" name="quantity" id="quantity" value="1" max="100" min="1">
                    <input type="hidden" class="inputProduct" name="products" value="<?php echo $pid; ?>">
                    <input type="button" value="+" class="incrementbtn" name="+">
                    <input type="button" value="-" class="decrementbtn" name="-">
                  </td>
                  <td id="remove">Remove</td>
                </tr>
          <?php

              }
            }
            ?>
            </table>
            <div style="padding-bottom:5%;">
              <h4>Send Quotes In Email</h4>
              <label for="senderNameID">Sender Name:</label>
              <input type="text" name="senderName" id="senderNameID " required>
              <label for="receiverEmailID">Reveivers Email:</label>
              <input type="text" name="receiverEmail" id="receiverEmailID" required><br>
              <input type="submit" class="sendMail" name="submit" value="Send">
            </div>
            <?php

          }
          if (empty($_SESSION["quote_products"])) {
            echo '<span style="font-size:20px;">Add Some Quote Requests Go Back to </span><a href=' . get_site_url() . '/online-shopping-in-pakistan-industrial-consumables-7mart-pk' . '>HomePage</a>';
          }


          ?>
         

      </form>
    </div>
  </div>

  <!-- Right Sidebar -->
  <?php if ($page_column_class['right_sidebar']) : ?>
    <aside id="right-sidebar" class="ts-sidebar <?php echo esc_attr($page_column_class['right_sidebar_class']); ?>">
      <?php if (is_active_sidebar($boxshop_page_datas['ts_right_sidebar'])) : ?>
        <?php dynamic_sidebar($boxshop_page_datas['ts_right_sidebar']); ?>
      <?php endif; ?>
    </aside>
  <?php endif; ?>

</div>

<?php get_footer();
if (!empty($_POST)) {

  $products = $_POST["products"];
  $_SESSION["quote_products"] = $products;
}

?>


<script type="text/javascript">
  jQuery(document).ready(function() {


    jQuery("tr input.incrementbtn").on("click", function() {
      var val = jQuery(this).closest("tr.tableData").find("input[id='quantity']").val();

      val++;
      jQuery(this).closest("tr.tableData").find("input[id='quantity']").val(val);

    });
    jQuery("tr input.decrementbtn").on("click", function() {
      var val = jQuery(this).closest("tr.tableData").find("input[id='quantity']").val();

      val--;
      if (val <= -1) {
        val = val * -1;
        jQuery(this).closest("tr.tableData").find("input[id='quantity']").val(val);

      }
      jQuery(this).closest("tr.tableData").find("input[id='quantity']").val(val);

    });
  });
</script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/js/jquery.cookie.js'; ?>"></script>

<script type="text/javascript">

    jQuery("div input.sendMail").on("click", function() {

      jQuery("table tr").each(function(index, element) {
        var qty = jQuery(element).find('input[name="quantity"]').val();
        var pId = jQuery(element).find('input[name="products"]').val();
        jQuery('#formID').append('<input type="hidden" name="products[]" value="' + pId + '|' + qty + '" />');
      });
      
    });
    jQuery("table td#remove").click(function() {
      jQuery(this).closest("tr").remove();
     });
    

</script>
