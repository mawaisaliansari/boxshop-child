<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );

		woocommerce_quantity_input( array(
			'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
			'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
		) );

		do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>

    <a href="#open-modal" class="button request-button" id="addToQuoteBtn" onclick="add_to_quote_list(event);">
	Request A Quote
	</a>

		<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
		
	</form>
	<form method="POST" id="inquiryform" action="<?php echo get_site_url().'/request-quotations'; ?>">
	<input type="hidden" class="inputProduct" name="products" >
	<div id="open-modal" class="modal-window">
  <div class="modal-div">
    <a href="#" title="Close" class="modal-close">Close</a>
    <h1>Quote Added!</h1>
    <div>You Have Added A Quote Click Below To Vist Quotes Page</div>
    <div><small>Check out</small></div>
	<input type="submit" value="View Quotes">
    </div>
	</div>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
  
<script type="text/javascript">
  jQuery(document).ready(function(){

    var rQproducts = jQuery.cookie("quotation_requested_products");
    //var pCounts = 0;
    if(rQproducts){
     // var pElements =  rQproducts.split(",");
    //  pCounts = pElements.length;
    } 
    jQuery('form input.inputProduct[name="products"]').val(rQproducts);
   // jQuery("div.query_box_wrapper div.query_count").html(pCounts);
	
  });
</script>
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script> -->
<!-- <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/js/jquery.cookie.js'; ?>"></script>
<script type="text/javascript">
function add_to_quote_list(eventVar){

var product_id = jQuery(eventVar.target).next().attr("value");

if(product_id){
  var cookieVal = jQuery.cookie("quotation_requested_products");
  if(cookieVal){
	var cookieArray = cookieVal.split(",");
	cookieArray[cookieArray.length] = product_id;
  }
  var insert = 1;
  if(cookieVal){
	if(!cookieVal.includes(product_id)){
	  jQuery.cookie("quotation_requested_products",cookieArray, { expires: 7, path: '/' });
	  var count = parseInt(jQuery("div.query_count").html());
	  count = count +1;
	  jQuery("div.query_count").html(count);
	}
	insert = 0;
  }
  
  

  if(insert){
	jQuery.cookie("quotation_requested_products",[product_id], { expires: 7, path: '/' });
	var count = parseInt(jQuery("div.query_count").html());
	count = count +1;
	jQuery("div.query_count").html(count);

  }
}



}
</script> -->